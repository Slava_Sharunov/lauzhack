from logipy import logi_led
import time
import threading
import numpy as np
import math
from random import randint

class KeyBoard:
    def __init__(self, currred=0, currgreen=0,currblue = 0):
        self.currred = currred
        self.currgreen = currgreen
        self.currblue = currblue
        self.emotions = {u'sadness': 0.000361910759, u'neutral': 0.9974811, u'contempt': 0.00104118488, u'disgust': 0.0, u'anger': 0.000157218485, u'surprise': 0.0009001187, u'fear': 1.0073768e-05, u'happiness': 4.189765e-05}
        self.thread = threading.Thread(target=self.__run__, args=())
        self.thread.daemon = True  # Daemonize thread

    def close(self):
        self.running = False

    def run(self):
        self.running = True
        self.thread.start()   # Start the execution

    def __run__(self):
        while self.running:
            self.emotionColor()
            time.sleep(1)
    def update(self, emotions):
        self.emotions = emotions
    def makeLight(self, red, green, blue):
        #print self.currred
        #print self.currgreen
        #print self.currblue
       # print red
        #print green
        #print blue
        redset = False
        greenset = False
        blueset = False
        logi_led.logi_led_init()
        for i in range(0, 100):
            if (not redset):
                if (red >= self.currred):
                    x = self.currred + 1
                else:
                    x = self.currred - 1
            if (not greenset):
                if (green >= self.currgreen):
                    y = self.currgreen + 1
                else:
                    y = self.currgreen - 1
            if (not blueset):
                if (blue >= self.currblue):
                    z = self.currblue + 1
                else:
                    z = self.currblue - 1
            if (x == red):
                x = red
                redset = True
            if (y == green):
                y = green
                greenset = True
            if (z == blue):
                z = blue
                blueset = True
            logi_led.logi_led_set_lighting(x, y, z)
            self.currred = x
            self.currgreen = y
            self.currblue = z
            time.sleep(0.006)
            if(blueset and redset and greenset): break

    def emotionColor(self):
        anger = [100,0,0]
        sadness = [0,0,100]
        contempt = [100,50,50]
        disgust = [0,100,0]
        surprise = [100,0,66]
        fear = [1,1,1]
        happiness = [100,66,0]
        neutral = [100,100,100]
        colors = [0,0,0]
        afraid = 1
        for item in self.emotions:
            if item == 'anger':
                if float(self.emotions.get(item)) > 0.6:
                    self.makeLight(*anger)
                    return
                colors += np.asarray(anger)*float(self.emotions.get(item))
                    #print('Hi, I`m anger')

            if item == 'sadness':
                if float(self.emotions.get(item)) > 0.8:
                    self.makeLight(*sadness)
                    return
                colors += np.asarray(sadness)*float(self.emotions.get(item))
                    #print('Hi, I`m sadness')

            if item == 'contempt':
                if float(self.emotions.get(item)) > 0.8:
                    self.makeLight(*contempt)
                    return
                colors += np.asarray(contempt)*float(self.emotions.get(item))
                    #print('Hi, I`m contempt')

            if item == 'disgust':
                if float(self.emotions.get(item)) > 0.8:
                    self.makeLight(*disgust)
                    return
                colors += np.asarray(disgust)*float(self.emotions.get(item))
                    #print('Hi, I`m disgust')

            if item == 'surprise':
                if float(self.emotions.get(item)) > 0.8:
                    self.makeLight(*surprise)
                    return
                colors += np.asarray(surprise)*float(self.emotions.get(item))
                    #print('Hi, I`m surprise')

            if item == 'fear':
                afraid = 1 - float(self.emotions.get(item))
                #colors += np.asarray(fear)*float(self.emotions.get(item))
                    #print('Hi, I`m fear')

            if item == 'happiness':
                if float(self.emotions.get(item)) > 0.8:
                    self.makeLight(*happiness)
                    return
                colors += np.asarray(happiness)*float(self.emotions.get(item))
                    #print('Hi, I`m happiness')

            if item == 'neutral':
                #if float(self.emotions.get(item)) > 0.8:
                 #   self.makeLight(100,0,0)
                 #   return
                colors += np.asarray(neutral)*float(self.emotions.get(item))

                    #print('Hi, I`m neutral')
        length = np.sqrt(colors)
        colors = colors/length
        maxim =  max(colors)
        magic = 1/maxim
        colors = colors*magic
        colors = colors*100
        colors = colors*afraid
        #print colors
        if(not math.isnan(colors[0]) and not math.isnan(colors[1]) and not math.isnan(colors[2])):
            self.makeLight(int(colors[0]), int(colors[1]), int(colors[2]))