import requests
import cv2
import base64
import numpy as np
from PIL import Image
from io import BytesIO
import time
import threading
from threading import Thread
import operator


class imageEmotion:
    def __init__(self, threshold=0.00001):

        self._url = 'https://api.projectoxford.ai/emotion/v1.0/recognize'
        self._key = 'ffac8973390c4308ba7ace9c3764ce1f'
        self._maxNumRetries = 20

        self.headers = dict()
        self.headers['Ocp-Apim-Subscription-Key'] = self._key
        self.headers['Content-Type'] = 'application/octet-stream'

        self.threshold = threshold

    def getEmotion(self, img_data):

        json = None
        params = None

        result = self.__processRequest__( json, img_data, self.headers, params )
               
        if result is not None:
            for currFace in result:
                emotions_Dictionary = currFace['scores']

               
                
        for key in emotions_Dictionary:
            if emotions_Dictionary[key]<self.threshold:
                emotions_Dictionary[key]=0.0  
            
        return emotions_Dictionary   

    def __processRequest__( self,json, data, headers, params ):

        """
        Helper function to process the request to Project Oxford

        Parameters:
        json: Used when processing images from its URL. See API Documentation
        data: Used when processing image read from disk. See API Documentation
        headers: Used to pass the key information and the data type request
        """

        none_Dictionary=[{'scores':
                          {'surprise': 0.0,
                           'fear': 0.0,
                           'disgust': 0.0,
                           'contempt': 0.0,
                           'anger': 0.0,
                           'sadness': 0.0,
                           'happiness': 0.0,
                           'neutral': 0.0}}]


        retries = 0
        result = none_Dictionary

        while True:

            response = requests.request( 'post', self._url, json = json, data = data, headers = headers, params = params )

            if response.status_code == 429: 

                print( "Message: %s" % ( response.json()['error']['message'] ) )

                if retries <= _maxNumRetries: 
                    time.sleep(1) 
                    retries += 1
                    continue
                else: 
                    print( 'Error: failed after retrying!' )
                    break

            elif response.status_code == 200 or response.status_code == 201:

                if 'content-length' in response.headers and int(response.headers['content-length']) == 0: 
                    result = none_Dictionary
                    print(result)
                elif 'content-type' in response.headers and isinstance(response.headers['content-type'], str): 
                    if 'application/json' in response.headers['content-type'].lower(): 
                        result = response.json() if response.content else none_Dictionary
                        if (result == []): result = none_Dictionary
                    elif 'image' in response.headers['content-type'].lower(): 
                        result = response.content
            else:
                try:
                    pass
                except:
                    print( "Error code: %d" % ( response.status_code ) )
                    print( "Message: %s" % ( response.json()['error']['message'] ) )

            break
            
        return result
   

cap = cv2.VideoCapture(0)
 

while 1:

    _, img = cap.read()
    if (time.gmtime()[5] % 3 == 0):
        img_jpg = Image.fromarray(img)
        fd = BytesIO()
        img_jpg.save(fd, 'JPEG')
        hex_data = fd.getvalue()
    cv2.imshow('img',img)
    if (time.gmtime()[5] % 6 == 0):
        IE = imageEmotion()
        emotion_dictionary = IE.getEmotion(hex_data)
        print(emotion_dictionary)
        currEmotion = max(emotion_dictionary.items(), key=operator.itemgetter(1))[0]
        print(currEmotion)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()  
    









 
 
 

