#!/usr/bin/env python3
import requests

import time
import threading
import json
import datetime
# NOTE: this example requires PyAudio bec-ause it uses the Microphone class

import speech_recognition as sr

class speechRecog:
    def __init__(self):
        self.thread = threading.Thread(target=self.__run__, args=())
        self.thread.daemon = True  # Daemonize thread

    def close(self):
        self.running = False

    def run(self):
        self.running = True
        self.thread.start()  # Start the execution

    def __run__(self):
        while self.running:
            self.recognize()
            time.sleep(5)
    def recognize(self):
        # obtain audio from the microphone
        r = sr.Recognizer()
        with sr.Microphone() as source:
            #print("Say something!")
            audio = r.listen(source)
            time.sleep(5)

        try:

            url = 'http://192.33.205.74:8000/wsm/default/log_emotion'
            dict = {}
            locTime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            sountText = r.recognize_sphinx(audio)
            dict['text'] = {"text": sountText, "time": locTime}
            payload = dict
            # GET
            # r = requests.get(url)
            # GET with params in URL
            # r = requests.get(url, params=payload)
            # POST with form-encoded data
            # r = requests.post(url, data=payload)
            # POST with JSON
            # requests.post(url, data=json.dumps(payload))
            print sountText

            r = requests.post(url, data=json.dumps(payload))

        except sr.UnknownValueError:
            print("Sphinx could not understand audio")
        except sr.RequestError as e:
            print("Sphinx error; {0}".format(e))
        # recognize speech using Google Speech Recognition
       # try:
            # for testing purposes, we're just using the default API key
            # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
            # instead of `r.recognize_google(audio)`
            #print("Google Speech Recognition thinks you said " + r.recognize_google(audio))
           # url = 'http://192.33.205.74:8000/wsm/default/log_emotion'
           # dict = {}
          #  locTime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
          #  sountText = r.recognize_google(audio)
          #  dict['text'] = {"text": sountText, "time": locTime}
          #  payload = dict
            # GET
            # r = requests.get(url)
            # GET with params in URL
            # r = requests.get(url, params=payload)
            # POST with form-encoded data
            # r = requests.post(url, data=payload)
            # POST with JSON
            # requests.post(url, data=json.dumps(payload))
          #  print sountText

          #  r = requests.post(url, data=json.dumps(payload))

        #    return
       # except sr.UnknownValueError:
       #     print("Google Speech Recognition could not understand audio")
       # except sr.RequestError as e:
       #     print("Could not request results from Google Speech Recognition service; {0}".format(e))

        # recognize speech using Microsoft Bing Voice Recognition
        #BING_KEY = "eb65593337d34e5e9920a3e0425031be" # Microsoft Bing Voice Recognition API keys 32-character lowercase hexadecimal strings
       # try:
       #     print("Microsoft Bing Voice Recognition thinks you said " + r.recognize_bing(audio, key=BING_KEY))
       # except sr.UnknownValueError:
       #     print("Microsoft Bing Voice Recognition could not understand audio")
       # except sr.RequestError as e:
      #      print("Could not request results from Microsoft Bing Voice Recognition service; {0}".format(e))

