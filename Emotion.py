import requests

class imageEmotion:
    def __init__(self, threshold=0.00001):

        self._url = 'https://api.projectoxford.ai/emotion/v1.0/recognize'
        self._key = 'ffac8973390c4308ba7ace9c3764ce1f'
        self._maxNumRetries = 10

        self.headers = dict()
        self.headers['Ocp-Apim-Subscription-Key'] = self._key
        self.headers['Content-Type'] = 'application/octet-stream'

        self.threshold = threshold

    def getEmotion(self, img_data):

        json = None
        params = None

        result = self.__processRequest__( json, data, self.headers, params )
        if result is not None:
            for currFace in result:
                emotions_Dictionary = currFace['scores']
                
        for key in emotions_Dictionary:
            if emotions_Dictionary[key]<self.threshold:
                emotions_Dictionary[key]=0.0  
            
        return sorted(emotions_Dictionary.items())
            

    def __processRequest__( self,json, data, headers, params ):

        """
        Helper function to process the request to Project Oxford

        Parameters:
        json: Used when processing images from its URL. See API Documentation
        data: Used when processing image read from disk. See API Documentation
        headers: Used to pass the key information and the data type request
        """

        retries = 0
        result = None

        while True:

            response = requests.request( 'post', self._url, json = json, data = data, headers = headers, params = params )

            if response.status_code == 429: 

                print( "Message: %s" % ( response.json()['error']['message'] ) )

                if retries <= _maxNumRetries: 
                    time.sleep(1) 
                    retries += 1
                    continue
                else: 
                    print( 'Error: failed after retrying!' )
                    break

            elif response.status_code == 200 or response.status_code == 201:

                if 'content-length' in response.headers and int(response.headers['content-length']) == 0: 
                    result = None 
                elif 'content-type' in response.headers and isinstance(response.headers['content-type'], str): 
                    if 'application/json' in response.headers['content-type'].lower(): 
                        result = response.json() if response.content else None 
                    elif 'image' in response.headers['content-type'].lower(): 
                        result = response.content
            else:
                print( "Error code: %d" % ( response.status_code ) )
                print( "Message: %s" % ( response.json()['error']['message'] ) )

            break
            
        return result




pathToFileInDisk = r'1.jpg'
with open( pathToFileInDisk, 'rb' ) as f:
    data = f.read()

IE = imageEmotion()

emotion_dictionary = IE.getEmotion(data)


 
 
 

