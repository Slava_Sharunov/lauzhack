'''
Created on 19.11.2016

@author: rufuslobo
'''
import time
from random import randint
import threading


class PulseEmulator():
    def __init__(self, basePulse=72, dbg=False):
        self.basePulse = basePulse # Basic Human Pulse in bpm
        self.dbg = dbg
        self.outputPulse = basePulse

        self.thread = threading.Thread(target=self.__run__, args=())
        self.thread.daemon = True                 # Daemonize thread


    def getPulse(self):
        return self.outputPulse


    def debugprint(self, msg):
        if self.dbg:
            print str(msg)

    def close(self):
        self.running = False

    def run(self):
        self.running = True
        self.thread.start()   # Start the execution

    def __run__(self):
        self.debugprint("Phase 1 - Indifference")
        timeCount = 1
        while self.running:
            while timeCount < 10:
                time.sleep(1)
                self.outputPulse = self.basePulse + randint(-1, 1)
                timeCount = timeCount + 1
                self.debugprint(self.outputPulse)

            # Phase 2 - Gradual Excitement
            self.debugprint("Phase 2 - Gradual Excitement")
            self.outputPulse = self.basePulse
            while self.outputPulse < 100:
                time.sleep(1)
                self.outputPulse = self.outputPulse + randint(-1, 5)
                timeCount = timeCount + 1
                self.debugprint(self.outputPulse)

            # Phase 3 - Settling Down
            self.debugprint("Phase 3 - Settling Down")
            while self.outputPulse > 65:
                time.sleep(1)
                self.outputPulse = self.outputPulse - randint(1, 2)
                timeCount = timeCount + 1
                self.debugprint(self.outputPulse)

            # Phase 4 - Getting Super Excited
            self.debugprint("Phase 4 - Getting Super Excited")
            while self.outputPulse < 160:
                time.sleep(1)
                self.outputPulse = self.outputPulse + randint(2, 7)
                timeCount = timeCount + 1
                self.debugprint(self.outputPulse)

            # Phase 5 - Calming down
            self.debugprint("Phase 5 - Calming down")
            while self.outputPulse > 72:
                time.sleep(1)
                self.outputPulse = self.outputPulse - randint(4, 6)
                timeCount = timeCount + 1
                self.debugprint(self.outputPulse)