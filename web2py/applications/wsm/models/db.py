# -*- coding: utf-8 -*-
from datetime import datetime

import numpy as np

import plotly.plotly as py
import plotly.tools as tls
import plotly.graph_objs as go

from plotly.graph_objs import *

import plotly
plotlyUser = 'Denise1' # 'madsen'
stream_id1 = 'gu8ddvwfet' #'kh5cdv5o6r'
# stream_id2 = 'ztrku7y5o2' #'831na4sh9t'
api_key= '5wfbdhe30k' # '4an5ldmv9t'
plotly.tools.set_credentials_file(username=plotlyUser, api_key=api_key, stream_ids=[stream_id1])
#
# stream_1live = go.Stream(
#     token=stream_id1,  # link stream id to 'token' key
#     maxpoints=50  # keep a max of 80 pts on screen
# )
#
# trace1live = go.Scatter(
#     x=[],
#     y=[],
#     name='heartbeat',
#     mode='lines+markers',
#     stream=stream_1live
# )
#
# dataLive = go.Data([trace1live])
#
# # Add title to layout object
# layoutLive = go.Layout(title='Heartbeat LIVE')
#
# # Make a figure object
# hbfigLive = go.Figure(data=dataLive, layout=layoutLive)
#
# # Send fig to Plotly, initialize streaming plot, open new tab
# py.plot(hbfigLive, filename='heartbeat-live', auto_open=False)

#import plotly
#stream_id = '831na4sh9t'
#api_key= '4an5ldmv9t'
#plotly.tools.set_credentials_file(username='madsen', api_key=api_key, stream_ids=[stream_id])


# -------------------------------------------------------------------------
# This scaffolding model makes your app work on Google App Engine too
# File is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

if request.global_settings.web2py_version < "2.14.1":
    raise HTTP(500, "Requires web2py 2.13.3 or newer")

# -------------------------------------------------------------------------
# if SSL/HTTPS is properly configured and you want all HTTP requests to
# be redirected to HTTPS, uncomment the line below:
# -------------------------------------------------------------------------
# request.requires_https()

# -------------------------------------------------------------------------
# app configuration made easy. Look inside private/appconfig.ini
# -------------------------------------------------------------------------
from gluon.contrib.appconfig import AppConfig

# -------------------------------------------------------------------------
# once in production, remove reload=True to gain full speed
# -------------------------------------------------------------------------
myconf = AppConfig(reload=True)

if not request.env.web2py_runtime_gae:
    # ---------------------------------------------------------------------
    # if NOT running on Google App Engine use SQLite or other DB
    # ---------------------------------------------------------------------
    db = DAL(myconf.get('db.uri'),
             pool_size=myconf.get('db.pool_size'),
             migrate_enabled=myconf.get('db.migrate'),
             check_reserved=['all'])
else:
    # ---------------------------------------------------------------------
    # connect to Google BigTable (optional 'google:datastore://namespace')
    # ---------------------------------------------------------------------
    db = DAL('google:datastore+ndb')
    # ---------------------------------------------------------------------
    # store sessions and tickets there
    # ---------------------------------------------------------------------
    session.connect(request, response, db=db)
    # ---------------------------------------------------------------------
    # or store session in Memcache, Redis, etc.
    # from gluon.contrib.memdb import MEMDB
    # from google.appengine.api.memcache import Client
    # session.connect(request, response, db = MEMDB(Client()))
    # ---------------------------------------------------------------------

# -------------------------------------------------------------------------
# by default give a view/generic.extension to all actions from localhost
# none otherwise. a pattern can be 'controller/function.extension'
# -------------------------------------------------------------------------
response.generic_patterns = ['*'] if request.is_local else []
# -------------------------------------------------------------------------
# choose a style for forms
# -------------------------------------------------------------------------
response.formstyle = myconf.get('forms.formstyle')  # or 'bootstrap3_stacked' or 'bootstrap2' or other
response.form_label_separator = myconf.get('forms.separator') or ''

# -------------------------------------------------------------------------
# (optional) optimize handling of static files
# -------------------------------------------------------------------------
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

# -------------------------------------------------------------------------
# (optional) static assets folder versioning
# -------------------------------------------------------------------------
# response.static_version = '0.0.0'

# -------------------------------------------------------------------------
# Here is sample code if you need for
# - email capabilities
# - authentication (registration, login, logout, ... )
# - authorization (role based authorization)
# - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
# - old style crud actions
# (more options discussed in gluon/tools.py)
# -------------------------------------------------------------------------

from gluon.tools import Auth, Service, PluginManager

# host names must be a list of allowed host names (glob syntax allowed)
auth = Auth(db, host_names=myconf.get('host.names'))
service = Service()
plugins = PluginManager()

# -------------------------------------------------------------------------
# create all tables needed by auth if not custom tables
# -------------------------------------------------------------------------
auth.define_tables(username=False, signature=False)

# -------------------------------------------------------------------------
# configure email
# -------------------------------------------------------------------------
mail = auth.settings.mailer
mail.settings.server = 'logging' if request.is_local else myconf.get('smtp.server')
mail.settings.sender = myconf.get('smtp.sender')
mail.settings.login = myconf.get('smtp.login')
mail.settings.tls = myconf.get('smtp.tls') or False
mail.settings.ssl = myconf.get('smtp.ssl') or False

# -------------------------------------------------------------------------
# configure auth policy
# -------------------------------------------------------------------------
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

# -------------------------------------------------------------------------
# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.
#
# More API examples for controllers:
#
# >>> db.mytable.insert(myfield='value')
# >>> rows = db(db.mytable.myfield == 'value').select(db.mytable.ALL)
# >>> for row in rows: print row.id, row.myfield
# -------------------------------------------------------------------------
db.define_table('livestats',
                Field('username', 'string'),
                Field('logtime', 'datetime'),
                Field('game', 'string'),
                Field('milestone', 'string'),
                Field('sess', 'integer'),
                Field('neutral', 'double'),
                Field('sadness', 'double'),
                Field('happiness', 'double'),
                Field('disgust', 'double'),
                Field('anger', 'double'),
                Field('surprise', 'double'),
                Field('fear', 'double'),
                Field('contempt', 'double'),
                Field('heartbeat', 'double'),
                Field('islive', 'string'),
                primarykey=['username']
                )

db.define_table('heartbeat',
                Field('logtime', 'datetime'),
                Field('username', 'string'),
                Field('game', 'string'),
                Field('milestone', 'string'),
                Field('sess', 'integer'),
                Field('heartbeat', 'double'),
                )


db.define_table('textreading',
                Field('logtime', 'datetime'),
                Field('username', 'string'),
                Field('game', 'string'),
                Field('milestone', 'string'),
                Field('sess', 'integer'),
                Field('textinput', 'text'),
                )


db.define_table('emotions',
                Field('logtime', 'datetime'),
                Field('username', 'string'),
                Field('game', 'string'),
                Field('milestone', 'string'),
                Field('sess', 'integer'),
                Field('neutral', 'double'),
                Field('sadness', 'double'),
                Field('happiness', 'double'),
                Field('disgust', 'double'),
                Field('anger', 'double'),
                Field('surprise', 'double'),
                Field('fear', 'double'),
                Field('contempt', 'double'),
                )

#db.livestats.insert(username='slava', face_etype='happy', face_econf='0.7', speech_etype='to be decided', speech_trans='What the f**k', bpm='110', is_live='yes')
#db.commit

# -------------------------------------------------------------------------
# after defining tables, uncomment below to enable auditing
# -------------------------------------------------------------------------
auth.enable_record_versioning(db)


# db.heartbeat.insert(logtime=datetime.strptime('2016-11-20 02:47:04', '%Y-%m-%d %H:%M:%S'), heartbeat=90)
# db.commit
